from django.conf.urls import url, include

from . import views

app_name='makemerich.main'
urlpatterns = [
    url(r'^memo/?', views.memo, name='memo'),
    url(r'^updateobs/?', views.updateobs, name='updatedb'),
    url(r'^full/?', views.full, name='full'),
    url(r'^recurrent/?', views.recurrent, name='recurrent'),
    url(r'^filldb/', views.fill_db, name='filldb'),
    url(r'', views.index, name='index'),
]
