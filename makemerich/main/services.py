import codecs
import ofxparse
from .models import Account, Statement, Transaction
from django.db import transaction
from django.db.utils import IntegrityError
import re
import logging
from dateutil.relativedelta import relativedelta as tdlt

log = logging.getLogger(__name__)

def fill_db(fil):

    ofx = ofxparse.OfxParser.parse(fil)

    account = Account.objects.get_or_create(
            institution=ofx.account.institution.organization,
            number=ofx.account.number)[0]

    statement = Statement.objects.get_or_create(
            account=account,
            balance=ofx.account.statement.balance,
            currency=ofx.account.statement.currency,
            date=ofx.account.statement.end_date)[0]


    def normatize_transaction(t):
        memo = t.memo
        memo = re.sub(r'\d{2}/\d{2}', '', memo)
        memo = re.sub(r'\d{2}:\d{2}', '', memo)
        memo = re.sub(r'\s+', ' ', memo)
        t.memo = memo

    for t in ofx.account.statement.transactions:
        normatize_transaction(t)
        try:
            Transaction.objects.get(date=t.date,
                                    amount=t.amount,
                                    memo=t.memo)
        except Transaction.DoesNotExist:
            Transaction.objects.create(amount=t.amount,
                                       date=t.date,
                                       memo=t.memo,
                                       statement=statement)


# Not being used
#def recurrent_transactions(date):
#    m1, m2, m3 = date.month, (date - tdlt(months=1)).month, (date - tdlt(months=2)).month
#    m1, m2, m3 = ['{:02d}'.format(x) for x in (m1, m2, m3)]
#    result = Transaction.objects.raw('''
#        select id, memo, avg(total) as tavg , min(total) as tmin, max(total) as tmax, group_concat(month) as _month from
#            (select id, memo, sum(amount) as total, strftime('%m', date) as month from main_transaction group by memo, month)
#            group by memo having _month like '%{},{},{}' order by tavg
#        '''.format(m3, m2, m1))
#    return result
