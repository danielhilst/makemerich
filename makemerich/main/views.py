from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST, require_GET
from django.db.models import Sum, Count
from django.http import HttpResponseNotFound
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta as r
from django.urls import reverse

from rest_framework.views import APIView
from rest_framework.response import Response

from . import services
from .forms import UploadFileForm
from .models import Account, Statement, Transaction

@require_POST
def fill_db(req):
    form = UploadFileForm(req.POST, req.FILES)                                                              
    if form.is_valid():                                                                                     
        fil = list(req.FILES.values())[0]                                                                   
        if fil.name.split('.')[-1] != 'ofx':                                                                
            form.add_error(field='file', error=ValidationError('Invalid format, it must be an .oxf file'))  
            return render(req, 'main/intex.html', context={ 'form': form })
        else:                                                                                               
            services.fill_db(fil)
    return redirect('/')

def __getdate(req):
    if 'date' in req.GET:
        _date = datetime.strptime(req.GET['date'], '%Y-%m-%d')
    else:
        _date = date.today() - r(months=1)
    return _date

def __context_dates(date):
    return {
        'date': date,     
        'date_prev': date - r(months=1),
        'date_next': date + r(months=1),
    }

@require_GET
def index(req):
    _date = __getdate(req)
    transactions = Transaction.services.month(_date).values('memo').annotate(amount=Sum('amount')).order_by('amount')
    total = Transaction.services.month_total(_date)
    total_in = Transaction.services.month_total_in(_date)
    total_out = Transaction.services.month_total_out(_date)
    try:
        total_growth = ', ({:0.2%})'.format((total[0].total - total[1].total) / abs(total[0].total))
        total_in_growth = ', ({:0.2%})'.format(total_in[0].total / total_in[1].total - 1.0)
        total_out_growth = ', ({:0.2%})'.format(total_out[0].total / total_out[1].total - 1.0)
    except (TypeError, IndexError, ZeroDivisionError):
        total_growth = ''
        total_in_growth = ''
        total_out_growth = ''

    try:
        return render(req, 'main/index.html', context=dict(
            form=UploadFileForm(),
            transactions=transactions,
            total='{:0.2f}{}'.format(total[0].total, total_growth),
            total_in='{:0.2f}{}'.format(total_in[0].total, total_in_growth),
            total_out='{:0.2f}{}'.format(total_out[0].total, total_out_growth),
            **__context_dates(_date)
        ))
    except IndexError:
        return render(req, 'main/index.html', context=dict(
            form=UploadFileForm(),
            tranasctions=transactions,
            total='',
            total_in='',
            total_out='',
            **__context_dates(_date),
        ))
   


@require_GET
def memo(req):
    _date = __getdate(req)
    memo = req.GET['memo']
    transactions = Transaction.services.month(_date).filter(memo=memo) \
            .values('id', 'date', 'memo', 'amount', 'obs').order_by('date')
    total = transactions.aggregate(total=Sum('amount'))

    return render(req, 'main/memo.html', context=dict(
        transactions=transactions,
        total=total,
        memo=memo,
        **__context_dates(_date)
    ))

@require_GET
def recurrent(req):
    _transactions = []
    _date = __getdate(req)
    total_avg, total_min, total_max = 0, 0, 0
    offset = -3
    transactions = Transaction.services.recurrent(_date, offset=offset)
    for t in transactions:
        t.history = reversed(t.history.split(','))
        _transactions.append(t)
        total_avg += t.tavg
        total_min += t.tmin
        total_max += t.tmax

    return render(req, 'main/recurrent.html', dict(
        transactions=_transactions,
        total_min=total_min,
        total_avg=total_avg,
        total_max=total_max,
        history_length=-offset,
        **__context_dates(_date)
    ))


def full(req):
    _date = __getdate(req)
    transactions = Transaction.services.month(_date).order_by('date')
    total = Transaction.services.month(_date).total()
    
    return render(req, 'main/full.html', dict(
        transactions=transactions,
        total=total,
        **__context_dates(_date)
    ))


@require_POST
def updateobs(req):
    print(req.POST)
    tid = req.POST['id']
    tobs = req.POST['obs']
    _date = datetime.strptime(req.POST['date'], '%Y-%m-%d')
    t = Transaction.objects.get(pk=tid)
    t.obs = tobs
    t.save()

    if req.POST['next'] == 'memo':
        ts = Transaction.services.month(_date).filter(memo=t.memo) \
                .values('id', 'date', 'memo', 'amount', 'obs').order_by('date')
        total = Transaction.services.month(_date).filter(memo=t.memo).aggregate(total=Sum('amount'))
        return render(req, 'main/memo.html', context=dict(
           transactions=ts,
           total=total,
           memo=t.memo,
           **__context_dates(_date)
        ))
    elif req.POST['next'] == 'full':
        t = Transaction.services.month(_date).values('id', 'date', 'memo', 'amount', 'obs')
        return render(req, 'main/full.html', dict(
            transactions=t,
            total=t.aggregate(total=Sum('amount'))['total']
            **__context_dates(_date)
        ))
    return redirect('/')




# Create your views here.

   
