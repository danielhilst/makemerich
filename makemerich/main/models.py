from django.contrib.auth.models import User
from django.db import models
from dateutil.relativedelta import relativedelta  as r
from datetime import date

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    history = models.IntegerField(default=3,
                                  help_text='Number of months permited')

class Account(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE, default=None)
    institution = models.CharField(max_length=32)
    number = models.CharField(max_length=32)
    class Meta:
        unique_together = ('profile', 'institution', 'number')


class Statement(models.Model):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    balance = models.DecimalField(max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=32)
    date = models.DateTimeField()
    class Meta:
        unique_together = ('account', 'balance', 'currency', 'date')


class Transaction(models.Model):
    statement = models.ForeignKey(Statement, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField()
    memo = models.CharField(max_length=100)
    obs = models.CharField(max_length=100, default='')
    class Meta:
        unique_together = ('amount', 'date', 'memo')

    def __repr__(self):
        return '#{} {} {} {}'.format(self.id, self.date, self.memo, self.amount)

    class QuerySet(models.QuerySet):
        _month_total_qry = '''select id, sum(amount) as total, strftime("%Y%m", date) as month 
                              from main_transaction 
                              where month between "{m1}" and "{m2}" {conds}
                              group by month order by month desc
                           '''

        @staticmethod
        def _daterg_fcty(datearg, offset, fmt='%Y-%m-%d'):
            d1 = date(datearg.year, datearg.month, 1) + r(months=1)
            d2 = d1 + r(months=offset) 
            args = [x.strftime(fmt) for x in (d2, d1)]
            return args

        def month(self, datearg=date.today(), offset=-1):
            args = type(self)._daterg_fcty(datearg, offset)
            return self.filter(date__range=args)

        def month_total(self, datearg=None, offset=-1):
            datearg = date.today() if datearg is None else datearg
            datearg = datearg - r(months=1)
            m1, m2 = self._daterg_fcty(datearg, offset, '%Y%m')
            qry = self._month_total_qry.format(m1=m1, m2=m2, conds='')
            res = self.raw(qry)
            return res

        def month_total_in(self, datearg=None, offset=-1):
            datearg = date.today() if datearg is None else datearg
            datearg = datearg - r(months=1)
            m1, m2 = self._daterg_fcty(datearg, offset, '%Y%m')
            qry = self._month_total_qry.format(m1=m1, m2=m2, conds='and amount > 0')
            return self.raw(qry)

        def month_total_out(self, datearg=None, offset=-1):
            datearg = date.today() if datearg is None else datearg
            datearg = datearg - r(months=1)
            m1, m2 = self._daterg_fcty(datearg, offset, '%Y%m')
            qry = self._month_total_qry.format(m1=m1, m2=m2, conds='and amount < 0')
            return self.raw(qry)

        def positive(self):
            return self.filter(amount__gt=0)

        def negative(self):
            return self.filter(amount__lt=0)

        def summation(self):
            return self.values('memo').annotate(amount=models.Sum('amount'))

        def like(self, contains):
            return self.filter(memo__contains=contains)

        def total(self):
            return self.aggregate(total=models.Sum('amount'))['total']

        def recurrent(self, date=date.today(), offset=-2):
            if offset >= 0:
                raise ValueError('offset must be negative')
            o = offset + 1 
            d = date + r(months=o)
            d_begin = d.strftime('%Y%m')
            d_end = date.strftime('%Y%m')
            mrange = ','.join([(date + r(months=i)).strftime('%Y%m') for i in range(o, 1)])
            print(mrange)
            q1 = '''
                 select id, memo, sum(amount) as amount, strftime('%Y%m', date) as month, strftime('%m-%Y', date) as pretty_month from {db_name}
                        where month between '{d_begin}' and '{d_end}' and amount < 0
                        group by memo, month
                '''.format(db_name=Transaction._meta.db_table,d_begin=d_begin, d_end=d_end)

            q = '''
                select id, memo, avg(amount) as tavg, min(amount) as tmin,
                        max(amount) as tmax, group_concat(pretty_month || ': ' || amount, ',') as history from
                    ({q})
                    group by memo having group_concat(month) like '%{mrange}' order by tavg
                '''.format(q=q1,
                           mrange=mrange)
            return self.raw(q)


    objects = models.Manager()
    services = QuerySet.as_manager()

