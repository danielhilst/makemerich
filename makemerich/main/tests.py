from django.test import TestCase, Client
from django.db import transaction
from django.db.models import Count, Sum
from django.urls import reverse
from unittest import mock
import codecs
from datetime import date
import os

# Create your tests here.

from .services import fill_db 
from .models import Transaction

def _filldb(): 
    paths = [f for f in os.listdir('.') if os.path.isfile(f) and 'ofx' in f]
    for f in paths:
        with codecs.open(f, 'r') as fil:
            fill_db(fil)

class TestModel(TestCase):

    def setUp(self):
        self.client = Client()

    def test_fill_db(self):
        from .models import Transaction
        with codecs.open('file1.ofx', 'r') as fil:
            fill_db(fil)
        total_1 = Transaction.objects.aggregate(Sum('amount'))
        with codecs.open('file1.ofx', 'r') as fil:
            fill_db(fil)
        total_2 = Transaction.objects.aggregate(Sum('amount'))
        print(total_1, total_2)
        self.assertTrue(len(Transaction.objects.all()) > 0)
        self.assertEqual(total_1['amount__sum'], total_2['amount__sum'])

    @mock.patch('makemerich.main.views.services.fill_db')
    def test_fill_db_view(self, fill_db):
        fill_db.return_value = None
        with codecs.open('file.ofx', 'r') as fil:
            response = self.client.post('/filldb/', { 'file': fil }, follow=True)
        self.assertEqual(response.status_code, 200)


    def test_recurrent_transactions(self):
        _filldb()
        for t in recurrent_transactions(date.today()):
            pass

    def test_recurrent_view(self):
        resp  = self.client.get('/recurrent')
        self.assertEqual(resp.status_code, 200)
            

    def test_transaction_custom_manager(self):
        _filldb()
        d = date(2017, 6, 1)
        t = [x for x in Transaction.services.month(offset=-2)]
        print(t[0].date, t[-1].date)
        print(Transaction.services.month().negative().summation().like('NIKK'))
        print(Transaction.services.month().negative().total())
        for t in Transaction.services.recurrent(date(2017,7,1)):
            print(t.memo, t.tavg, t.tmin, t.tmax, t.history)


    def test_months(self):
        _filldb()
        print(list(Transaction.services.month_total()))
        # print(Transaction.services.month_total_in())
        # print(Transaction.services.month_total_out())

class TestReverseApiView(TestCase):
    def test_reverse_transactions(self):
        reverse('makemerich.main:transaction-detail', kwargs={'pk': 1})
