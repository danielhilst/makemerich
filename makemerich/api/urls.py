from django.urls import include, path
from rest_framework import routers, schemas
from rest_auth.registration.views import (
    SocialAccountListView, SocialAccountDisconnectView,
)


from . import views


router = routers.SimpleRouter()
router.register('accounts', views.AccountViewSet)
router.register('statement', views.StatementViewSet)

auth_urlpatterns = [
    path('', include('rest_auth.urls')),
    path('facebook/', views.FacebookLogin.as_view(), name='facebook'),
    path('google/', views.GoogleLogin.as_view(), name='google'),
    path('socialaccounts/', SocialAccountListView.as_view(),
        name='social-account-list'),
    path('socialaccounts/<int:pk>/diconnect/',
        SocialAccountDisconnectView.as_view(),
        name='social-account-disconnect'),
    path('connect/facebook/', views.FacebookConnect.as_view(),
        name='facebook-connect'),
    path('connect/google/', views.GoogleConnect.as_view(),
        name='google-conect'),
    path('registration/', include('rest_auth.registration.urls')),
]

app_name='makemerich.api'
urlpatterns = [
    path('', include(router.urls)),
    path('', views.ApiRoot.as_view(), name='api-root'),
    path('auth/', include(auth_urlpatterns)),
    path('schema/', schemas.get_schema_view(title='makemerich API')),
    path('transactions/<int:year>/<int:month>/',
         views.TransactionList.as_view(),
         name='transaction-list'),
    path('transactions/<int:year>/',
         views.TransactionList.as_view(),
         name='transaction-list'),
    path('transactions/',
         views.TransactionList.as_view(),
         name='transaction-list'),
    path('transaction/<int:pk>/',
         views.TransactionDetail.as_view(),
         name='transaction-detail'),
    path('ofxupload',
         views.OFXUpload.as_view(),
         name='ofxupload'),
         
]
