from rest_framework import viewsets, generics, parsers, status, views
from rest_framework.reverse import reverse
from rest_framework.response import Response

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from rest_auth.registration.views import SocialLoginView, SocialConnectView


from ..main import models, services
from . import serializers

class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
    callback_url = 'http://localhost:8000/accounts/google/login/callback/'
    client_class = OAuth2Client


class FacebookConnect(SocialConnectView):
    adapter_class = FacebookOAuth2Adapter


class GoogleConnect(SocialConnectView):
    adapter_class = GoogleOAuth2Adapter


class AccountViewSet(viewsets.ModelViewSet):
    queryset = models.Account.objects.all()
    serializer_class = serializers.AccountSerializer


class StatementViewSet(viewsets.ModelViewSet):
    queryset = models.Statement.objects.all()
    serializer_class = serializers.StatementSerializer

class TransactionList(generics.ListAPIView):
    serializer_class = serializers.TransactionSerializer
    
    def get_queryset(self):
        try:
            return models.Transaction.objects.filter(date__month=self.kwargs['month'],
                                                     date__year=self.kwargs['year'])
        except KeyError:
            pass
        try:
            return models.Transaction.objects.filter(date__year=self.kwargs['year'])
        except KeyError:
            pass
        return models.Transaction.objects.all()


class TransactionDetail(generics.RetrieveAPIView):
    serializer_class = serializers.TransactionSerializer
    queryset = models.Transaction.objects.all()


class OFXUpload(views.APIView):
    parser_classes = (parsers.FileUploadParser,)
    def put(self, request, filename, format=None):
        file_obj = request.FILES['file']
        services.fill_db(file_obj)
        return Response(status=status.HTTP_204_NO_CONTENT)

class ApiRoot(views.APIView):
    def get(self, request):
        return Response({
            '/api/transactions/': reverse('makemerich.api:transaction-list', request=request),
            '/api/tranasctions/<int:year>/': reverse('makemerich.api:transaction-list', request=request, kwargs={'year': 2018}),
            '/api/tranasctions/<int:year>/<int:month>/': reverse('makemerich.api:transaction-list', request=request, kwargs={'year': 2018, 'month': 1}),
            '/api/accounts/': reverse('makemerich.api:account-list', request=request),
            '/api/statements/': reverse('makemerich.api:statement-list', request=request),
            '/api/ofxupload/': reverse('makemerich.api:ofxupload', request=request),
        })

