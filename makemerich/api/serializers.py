from rest_framework import serializers

from ..main import models

class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Account
        fields = '__all__'
        extra_kwargs = {
            'url': {
                'view_name': 'makemerich.api.v1:account-detail'
            }
        }

class StatementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Statement
        fields = '__all__'
        extra_kwargs = {
            'url': {
                'view_name': 'makemerich.api:statement-detail'
            },
            'account': {
                'view_name': 'makemerich.api:account-detail'
            }
        }

    
class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Transaction
        fields = '__all__'
        extra_kwargs = {
            'url': {
                'view_name': 'makemerich.api:transaction-detail'
            },
            'statement': {
                'view_name': 'makemerich.api:statement-detail',
            }
        }




