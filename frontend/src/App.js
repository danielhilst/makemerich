import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor() {
    super(arguments);
    this.state = {
      accessToken: null,
      loggedWith: null, // google | facebook | password
      authenticated: false,
    }
  }

  onFacebookLogin = data => {
    const body = JSON.stringify({
      'access_token': data.accessToken,
    });
    console.log(body, ' body');

    fetch('/api/auth/facebook/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/html, */*',
        'Content-Type': 'application/json',
      },
      body: body
    }).then(resp => {
      console.log(resp);
      if (!resp.ok) {
        resp.json().then(errors => {
          this.setState({
            error: true,
            errorMessage: errors.non_field_errors
          });
        })
        return;
      }
    });
  }

  onGoogleLogin = data => {
    this.setState({
      accessToken: data.accessToken,
    });
  }

  onChange = event => {
    console.log(event.target.name, 'change');
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="errors">{ this.state.errorMessage }</div>
        {
          this.state.authenticated || 
          <div>
            <form>
              <label>Username </label>
              <input type="text" name="username" onChange={this.onChange} />
              <label>Password </label>
              <input type="password" name="password" onChange={this.onChange} />
              <button type="submit">Submit</button>
              <button>Register</button>
            </form>
            <div>
              <FacebookLogin
                appId='277257176150135'
                autoLoad={true}
                fields='name,email,picture'
                onClick={console.log}
                callback={this.onFacebookLogin}
              />
            </div>
            <div>
              <GoogleLogin
                clientId='419312690453-o4grg1t8isrjbljfnnno7c7jj520fpao.apps.googleusercontent.com'
                buttonText='Login with Google'
                onSuccess={this.onGoogleLogin}
                onFailire={console.log}
              />
            </div>
          </div>
        }
      </div>
        );
  }
}

export default App;

// vim: set ts=2 sts=2 sw=2 et:
